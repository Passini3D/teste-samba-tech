(function($){
  $(function(){
  	
    $('.sidenav').sidenav();

    $.getJSON('https://www.breakingbadapi.com/api/characters?limit=10', function(data) {
        console.log(data);

		for (var i = 0; i < 9; i++) {
		   var item = {
		      img: data[i].img,
		      status: data[i].status,
		      name: data[i].name,
		      birthday: moment(data[i].birthday, 'YYYY-MM-DD').format('DD/MM/YYYY'),
		      occupation: data[i].occupation,
		    },
		    
		    template = $('#character-template').html();
		    Mustache.parse(template);
		    rendered = Mustache.render(template, item);
		    $('.characters').append(rendered); 
		}
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space
